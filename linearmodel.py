import torch

x_data = torch.Tensor([[1.0], [2.0], [3.0]])
y_data = torch.Tensor([[2.0], [4.0], [6.0]])


class LinearModel(torch.nn.Module):
    def __init__(self):
        super(LinearModel, self).__init__()
        self.linear = torch.nn.Linear(1, 1)

    def forward(self, x):
        return self.linear(x)


model = LinearModel()
criterion = torch.nn.MSELoss(reduction='sum')
optimizer = torch.optim.SGD(model.parameters(), lr=0.02)

for epoch in range(1000):
    y_pred = model(x_data)
    loss = criterion(y_pred, y_data)
    if epoch % 100 == 0:
        print(epoch + 1, loss)

    optimizer.zero_grad()
    loss.backward()
    optimizer.step()

print('w = {}'.format(model.linear.weight.item()))
print('b = {}'.format(model.linear.bias.item()))

x_test = torch.Tensor([[4.0]])
y_test = model(x_test)
print('y_pred(4.0) = ', y_test.data)

x_test = torch.Tensor([[8.0]])
y_test = model(x_test)
print('y_pred(8.0) = ', y_test.data)

x_test = torch.Tensor([[10.0]])
y_test = model(x_test)
print('y_pred(10.0) = ', y_test.data)

x_test = torch.Tensor([[15.0]])
y_test = model(x_test)
print('y_pred(15.0) = ', y_test.data)

model.eval()
dummy_input = torch.randn(1, 1)
input_name = ["input"]
output_name = ["output"]
onnx_name = "test.onnx"
torch.onnx.export(
    model,
    dummy_input,
    onnx_name,
    verbose=True,
    input_names=input_name,
    output_names=output_name
)