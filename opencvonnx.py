from cv2 import dnn
import numpy as np

net = dnn.readNetFromONNX("test.onnx")
matblob = np.full((1, 1), 1024, dtype=np.int32)
net.setInput(matblob)
print('input = {}'.format(matblob))
output = net.forward()
print('output = {}'.format(output))
