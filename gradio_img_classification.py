import torch
from PIL import Image
from torchvision import transforms
from torchvision.models import ResNet18_Weights
import gradio as gr
import json

with open('imagenet-simple-labels.json', 'r') as load_f:
    labels = json.load(load_f)
model = torch.hub.load("pytorch/vision:v0.6.0", "resnet18", weights=ResNet18_Weights.DEFAULT).eval()


def predict(inp):
    inp = Image.fromarray(inp.astype("uint8"), "RGB")
    inp = transforms.ToTensor()(inp).unsqueeze(0)
    with torch.no_grad():
        prediction = torch.nn.functional.softmax(model(inp)[0], dim=0)
    return {labels[i]: float(prediction[i]) for i in range(1000)}


inputs = gr.Image()
outputs = gr.Label(num_top_classes=3)
demo = gr.Interface(fn=predict, inputs=inputs, outputs=outputs)

if __name__ == '__main__':
    demo.launch()
