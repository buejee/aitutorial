import matplotlib.pyplot as plt
import numpy as np
from PIL import Image
from skimage import transform
from sklearn import datasets

data = datasets.load_digits()

features = data['data']

img = Image.open("images/number_2.png")
img = img.resize((32, 32))
img = img.convert('L')

img_new = img.point(lambda x: 0 if x > 170 else 1)
arr = np.array(img_new)
for i in range(arr.shape[0]):
    print(arr[i])

'''
newfeatures = [transform.resize(features[i].reshape(8, 8), (32, 32)) for i in range(len(features))]
plt.imshow(newfeatures[1].reshape((32, 32)))
'''
new_img = transform.resize(arr.reshape(32, 32), (8, 8))
plt.imshow(new_img.reshape((8, 8)))

plt.show()
