import gradio as gr


def hello(name):
    return "hello," + name + "!"


def launch():
    demo = gr.Interface(fn=hello, inputs='text', outputs='text')
    demo.launch()


if __name__ == '__main__':
    launch()
