from keras.datasets import mnist
from keras import layers
from keras import models
from keras.utils.np_utils import to_categorical

(train_images, train_labels), (test_images, test_labels) = mnist.load_data()
network = models.Sequential()
network.add(layers.Flatten(input_shape=(28, 28, 1)))
network.add(layers.Dense(512, activation="relu"))
network.add(layers.Dense(10, activation="softmax"))
network.summary()

network.compile(optimizer="rmsprop", loss="categorical_crossentropy", metrics=["accuracy"])

train_images_nor = train_images.astype("float32") / 255
test_images_nor = test_images.astype("float32") / 255

train_labels_cate = to_categorical(train_labels)
test_labels_cate = to_categorical(test_labels)
print(f'Training Labels Shape: {train_labels_cate.shape}')
print(f'Testing Labels Shape: {test_labels_cate.shape}')

hist = network.fit(train_images_nor, train_labels_cate, epochs=5, batch_size=128)
test_loss, test_acc = network.evaluate(test_images_nor, test_labels_cate)
print('test accuracy: ', test_acc)
